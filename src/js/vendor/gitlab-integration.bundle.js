;(function () {
  'use strict'

  const { Gitlab } = require('@gitbeaker/browser')
  const ClientOAuth2 = require('client-oauth2')
  const Querystring = require('querystring')

  // ----------------------------------------

  let loginState = {}

  try {
    loginState = JSON.parse(window.localStorage.getItem('gitlab-login-state')) || {}
  } catch (err) {}

  // ----------------------------------------

  function buildClientOAuth2 () {
    return new ClientOAuth2({
      clientId: loginState.clientId,
      accessTokenUri: loginState.gitlabUrl + '/oauth/token',
      authorizationUri: loginState.gitlabUrl + '/oauth/authorize',
      redirectUri: loginState.redirectUrl,
      state: loginState.oauthRandomizedState,
      scopes: ['read_api'],
    })
  }

  // ----------------------------------------

  // Check to see if we are returning from GitLab with the token
  const queryParams = Querystring.parse(window.location.search.substr(1))
  if (queryParams.code) {
    buildClientOAuth2().code.getToken(window.location)
      .then(function (token) {
        const initialLocation = loginState.initialLocation

        loginState.gitlabToken = token.accessToken
        delete loginState.initialLocation
        delete loginState.oauthRandomizedState
        delete loginState.clientId
        delete loginState.gitlabUrl
        delete loginState.redirectUrl
        window.localStorage.setItem('gitlab-login-state', JSON.stringify(loginState))

        // Jump over to the specific page we were at
        window.location = initialLocation
      }).catch(function (err) {
        console.log('auth.code.getToken; err = ', err)
      })
  }

  // --------------------------------------------------------------------------------

  window.onload = async (evt) => {
    const loginDiv = document.body.querySelector('.gitlab-login')

    // ----------------------------------------
    // Try accessing GitLab

    // If we have an oauth2 token, try it out to see if it is still valid
    // If it works, we have a valid connection, and can stop here
    if (loginState.gitlabToken) {
      try {
        const gitlab = new Gitlab({
          host: 'https://gitlab.opengroup.org',
          oauthToken: loginState.gitlabToken,
        })

        const whoami = await gitlab.Users.current()

        // Build the avatar
        const avatar = document.createElement('img')
        avatar.className = 'avatar'
        avatar.src = whoami.avatar_url
        loginDiv.appendChild(avatar)

        // Store the GitLab object
        window.gitlab = gitlab
        return
      } catch (err) {
        // Token must be expired or invalid, clear this out and present a login button
        loginState.gitlabToken = null
        window.localStorage.setItem('gitlab-login-state', JSON.stringify(loginState))
      }
    }

    // ----------------------------------------
    // Build the login button

    const clientId = loginDiv.getAttribute('data-client-id')
    const gitlabUrl = loginDiv.getAttribute('data-url')
    const redirectUrl = loginDiv.getAttribute('data-redirect-url')

    if (clientId && gitlabUrl && redirectUrl) {
      const loginButton = document.createElement('span')
      loginButton.className = 'login-button'
      loginButton.innerHTML = 'Log In'
      loginButton.onclick = () => {
        loginState.initialLocation = window.location.href
        loginState.oauthRandomizedState = window.btoa(window.crypto.getRandomValues(new Uint32Array(4)))
        loginState.clientId = clientId
        loginState.gitlabUrl = gitlabUrl
        loginState.redirectUrl = redirectUrl
        window.localStorage.setItem('gitlab-login-state', JSON.stringify(loginState))

        window.location = buildClientOAuth2().code.getUri()
      }
      loginDiv.appendChild(loginButton)
    }
  }
})()
