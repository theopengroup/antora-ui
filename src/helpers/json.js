'use strict'

module.exports = (arg) => {
  return JSON.stringify(arg, null, 2)
}
