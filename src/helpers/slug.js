'use strict'

module.exports = (arg) => {
  if (typeof arg === 'string') {
    return arg.split(/\W+/)
      .filter((substr) => substr.length > 0)
      .map((substr) => substr.toLowerCase())
      .join('-')
  }
}
