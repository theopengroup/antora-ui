'use strict'

// Converts a createIssueUrl into a url to the corresponding project's issue board
// So, for the input: https://.../issues/new?template=...
// We convert to: https://.../boards
// We do this by throwing away the last two slash-separated components, then adding "/boards"
module.exports = (arg) => {
  if (typeof arg === 'string') {
    const parts = (arg || '').split('/')
    parts.pop()
    parts.pop()
    return parts.join('/') + '/boards'
  }
}
