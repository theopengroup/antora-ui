'use strict'

// Converts a createIssueUrl into a url to the corresponding project's issue list
// So, for the input: https://.../issues/new?template=...
// We convert to: https://.../issues
// We do this by throwing away the last slash-separated component
module.exports = (arg) => {
  if (typeof arg === 'string') {
    const parts = (arg || '').split('/')
    parts.pop()
    return parts.join('/')
  }
}
